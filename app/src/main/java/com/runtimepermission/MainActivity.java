package com.runtimepermission;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends BaseActivity {

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //This method will call the runtime permission request.
        requestPermissionAction();

        /**
         * Create a observer for @MutableLiveData<Integer> statusCode
         * when there is data change it will notify in onChanged
         *
         * NOTE : Integer description
         *
         * 1. Success.
         * 2. Failure.
         * */
        getStatusCode().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                Toast.makeText(MainActivity.this, "Permission status : "+integer, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
