package com.runtimepermission;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Copyright (c) Manoj
 *
 * Created by manoj on 26/2/18.
 *
 * This class help to check and request the runtime permission
 * that are required in the application to perform the specific
 * task.
 *
 * To active this just call the requestPermissionAction() method.
 *
 * To add the next set of permission add it in perms[] String array,
 * rest every thing will be taken care.
 *
 * If user deny all permission and check don't ask again display
 * a custom alert box, navigate to settings to enable permission
 * using startActivityForResult so that we can capture the event
 * after coming back and check again for permission available
 * or not.
 */

public abstract class PermissionActivity extends AppCompatActivity {

    String[] perms = {"android.permission.WRITE_EXTERNAL_STORAGE",
            "android.permission.CAMERA"};
    int permsRequestCode = 200;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("NewApi")
    public void requestPermissionAction(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(perms, permsRequestCode);
            }else {
                onPermissionsGranted(1);
            }
        }else {
            onPermissionsGranted(1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(permsRequestCode){
            case 200:
                Integer value = checkAllPermission();
                callCommonAction(value);
            break;
        }
    }

    private void callCommonAction(Integer value) {
        switch (value){
            case 999:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.instructions_to_turn_on_storage_permission)
                        .setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent settingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                settingsIntent.setData(uri);
                                startActivityForResult(settingsIntent, 7);
                            }
                        })
                        .setNegativeButton(getString(R.string.not_now), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                onPermissionsGranted(2);
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
                break;
            default:
                onPermissionsGranted(1);
                break;
        }
    }

    private Integer checkAllPermission() {
        Integer value = 0;
        int i=0;
        while (i < perms.length){
            if(ContextCompat.checkSelfPermission(this, perms[i]) != PackageManager.PERMISSION_GRANTED){
                value = 999;
                break;
            }
            i++;
        }
        return value;
    }

    private boolean canMakeSmores(){
        return(Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    private boolean hasPermission(String permission){
        if(canMakeSmores()){
            return(checkSelfPermission(permission)==PackageManager.PERMISSION_GRANTED);
        }
        return true;
    }

    public int checkSelfPermission(String permission) {
        return 1;
    }

    public abstract void onPermissionsGranted(int requestCode);

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 7:
                Integer value = checkAllPermission();
                callCommonAction(value);
                break;
        }
    }
    
}