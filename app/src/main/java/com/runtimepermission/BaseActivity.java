package com.runtimepermission;

import android.arch.lifecycle.MutableLiveData;
import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Copyright (c) Manoj
 *
 * Created by manoj on 09-02-2018.
 *
 * It's the common base extend class for all activity that
 * we create in this project.
 *
 * Define all the variables and method that we need it as a global
 * access for different activity.
 *
 * @BaseActivity extends @PermissionActivity all the runtime
 * permission are defined and declared in it. If we just want
 * to check or request the permission. requestPermissionAction()
 * method.
 *
 * Both @BaseActivity & @PermissionActivity are abstract so it's not
 * mandatory to Override all the abstract methods for Extending
 * Class @PermissionActivity. Hence @BaseActivity is not a Concert Class.
 */

public abstract class BaseActivity extends PermissionActivity {

    protected abstract int getLayoutResource();
    MutableLiveData<Integer> statusCode = new MutableLiveData<Integer>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try{
            setContentView(getLayoutResource());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onPermissionsGranted(int responseCode) {
        statusCode.postValue(responseCode);
    }

    public MutableLiveData<Integer> getStatusCode() {
        return statusCode;
    }
}